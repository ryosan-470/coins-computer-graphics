#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <GLUT/glut.h>

#define RAD 0.0174533// 3.14592654/180.
#define TRACKBALLSIZE  (0.8f)// 仮想トラックボールの大きさ
#define AXIS 1

int _w,_h;  // 描画ウィンドウ大きさ
float _aratio;// 描画ウィンドウ縦横比
float  mx0,my0,mx1,my1;// マウスドラッグ時の始点、終点座標
float mxsave[]={1.,0.,0.,0., 0.,1.,0.,0., 0.,0.,1.,0., 0.,0.,0.,1.};
float mx[]={1.,0.,0.,0., 0.,1.,0.,0., 0.,0.,1.,0., 0.,0.,0.,1.};

float mesh[4][4][3]={// ４×４の制御多角形の制御点群（3次元座標値を持つ）
  -3.,-2.,-2., -3.,-0.7,-1., -3.,0.7,0., -3.,2.,-1.,
  -1.,-2.,0., -1.,-0.7,1.3, -1.,0.7,0.8, -1.,2.,0.2,
  1.,-2.,0.5, 1.,-0.7,0.7, 1.,0.7,1.5, 1.,2.,0.9,
  3.,-2.,0., 3.,-0.7,0.3, 3.,0.7,0.6, 3.,2.,0.2 };

float  knots[]={0.,0.,0.,0.,1.,1.,1.,1.};  // NURBSの定義に必要なノット列

int dispCtrlPolygon=1; // 制御ポリゴンの表示（Cキーで切替）
int dispNurbs=1;        // 表示モード (0:メッシュ 1:曲面)
int dispAxis=1;        // 座標軸表示の有無　（Aキーで切替）

GLUnurbsObj *myNurbs; // NURBSオブジェクト

void drawMesh( float *w, int m, int n )
{
  // w: 縦×横の格子状の3次元座標の点列が入る配列の先頭アドレス
  // m: 縦方向の行数
  // n: 横方向の列数
  int i,j;
  for( i=0; i<m; i++ ){
    glBegin(GL_LINE_STRIP);
    for( j=0;j<n;j++ )glVertex3fv( w+(n*i+j)*3 );
    glEnd();
  }
  for( j=0; j<n; j++ ){
    glBegin(GL_LINE_STRIP);
    for( i=0;i<m;i++ )glVertex3fv( w+(n*i+j)*3 );
    glEnd();
  }
}

void display(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();
  glTranslatef( 0.,0.,-500. );
  glMultMatrixf( mx );
  glScalef( 50.,50.,50. );

  if( dispAxis )glCallList( AXIS );
  if( dispCtrlPolygon ){
    glColor3f( 0.,0.5,0. );
    glLineWidth(1.);
    drawMesh( &mesh[0][0][0],4,4 ); // 3次元座標値の制御点列のメッシュ(4×4)を描く
  }
  if( dispNurbs ){  // 曲面を表示
    glEnable(GL_LIGHTING);     // 面を表示するための光源（ここでは既定値の光源を使う）
    gluBeginSurface(myNurbs);  // NURBSオブジェクトの実体を生成する
    gluNurbsSurface(myNurbs, // GLUnurbsObj構造体へのポインタ
                    8, knots,              // u方向ノット数(3次Bezierの場合は8)
                    8, knots,              // v方向ノット数(3次Bezierの場合は8)
                    4 * 3,                 // u方向に次の制御点に移動するためのアドレス間隔
                    3,                     // v方向に次の制御点に移動するためのアドレス間隔
                    &mesh[0][0][0],        // 制御点が入っている配列の先頭アドレス
                    4, 4,                  // u,v方向の階数＝次数+1
                    GL_MAP2_VERTEX_3);     // 座標値の場合はこの値
    gluEndSurface(myNurbs);    // NURBSオブジェクト生成終了
    glDisable(GL_LIGHTING);    // 以降、線画を表示するために、光源を消す
  }
  glutSwapBuffers();
}

void vsub(const float *src1, const float *src2, float *dst)
{
  dst[0] = src1[0] - src2[0];
  dst[1] = src1[1] - src2[1];
  dst[2] = src1[2] - src2[2];
}

void vcross(const float *v1, const float *v2, float *cross)
{// ベクトル外積計算    cross[] <- v1[] × v2[]
  cross[0] = (v1[1] * v2[2]) - (v1[2] * v2[1]);
  cross[1] = (v1[2] * v2[0]) - (v1[0] * v2[2]);
  cross[2] = (v1[0] * v2[1]) - (v1[1] * v2[0]);
}

float vlength(const float *v) // 3次元ベクトルの長さを求める
{return (float)sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);}

void mxcopy( float s[], float d[] ) // src -> dst
{register int i;for( i=0; i<16; i++ ) d[i]=s[i]; }

void mxmultiply( float x[], float a[] )
{// 行列xに右側から行列aを掛ける
  float w[4][4];    // 作業用行列
  register int   i,j,k;

  for( i=0; i<4; i++ ){
    for( j=0; j<4; j++ ){ w[i][j]=0.;
      for( k=0; k<4; k++ ) w[i][j] += x[i*4+k]*a[k*4+j];
    }
  }
  for( i=0; i<4; i++ ){
    for( j=0; j<4; j++ ) x[i*4+j]=w[i][j];
  }
}

static float tb_project_to_sphere(float r, float x, float y)
{// 平面上の点(x,y)から仮想トラックボール上の(x,y,z)を求める
  float d, z;

  d = (float)sqrt(x*x + y*y);
  if (d < r * 0.7071) { // トラックボール上を仮定
    z = (float)sqrt(r*r - d*d);
  } else { // トラックボールを外れた場合、双曲面上を仮定
    z = r*r / (2.*d);
  }
  return z;
}

void axis_to_matrix(float t, float u, float v, float w, float x[])
{
  // (u,v,w)方向を回転軸としてtラディアンの回転変換行列xを作る
  float c, s, d;
  float uu,uv,uw,vv,vw,ww,c1,us,vs,ws;

  if( (d=(float)sqrt(u*u + v*v + w*w)) < 1.e-9 ){
    printf("no axis defined\n"); u=1.; v=w=0.;
  } else { u /= d; v /= d; w /= d; }
  uu=u*u; uv=u*v; uw=u*w;vv=v*v; vw=v*w;ww=w*w;
  c=(float)cos((double)t); s=(float)sin((double)t);
  c1 = 1. - c;us=u*s; vs=v*s; ws=w*s;
  x[0]=uu+(1.-uu)*c; x[4]=uv*c1-ws;     x[8]=uw*c1+vs;
  x[1]=uv*c1+ws;     x[5]=vv+(1.-vv)*c; x[9]=vw*c1-us;
  x[2]=uw*c1-vs;     x[6]=vw*c1+us;     x[10]=ww+(1.-ww)*c;
  x[3]=x[7]=x[11]=x[12]=x[13]=x[14]=0.;x[15]=1.;
}

int trackball(float m[16], float p1x, float p1y, float p2x, float p2y)
// マウスドラッグの始点(p1x,p1y)、終点(p2x,p2y)から回転変換行列mを求める
{
  float a[3];// 回転軸
  float phi;// 回転角（ラディアン単位）
  float p1[3], p2[3], d[3];
  float t;

  if( p1x==p2x && p1y==p2y ) return 0;
  p1[0] = p1x;p1[1] = p1y;
  p1[2] = tb_project_to_sphere(TRACKBALLSIZE, p1x, p1y);
  p2[0] = p2x;  p2[1] = p2y;
  p2[2] = tb_project_to_sphere(TRACKBALLSIZE, p2x, p2y);
  vcross(p2, p1, a);
  vsub(p1, p2, d);
  t = vlength(d) / (2.0f * TRACKBALLSIZE);

  // トラックボール外にカーソルが来た場合に制御不能にならないように
  // トラックボールが双曲面上に拡張されて存在しているものとみなす
  if (t>1.0)t = 1.0;
  if (t < -1.0)t = -1.0;
  phi = 2.0f * (float)asin(t);
  axis_to_matrix(phi, a[0],a[1],a[2], m);
  return 1;
}

void drawLines( float x[][3], int n )
{// 配列ｘに入っているｎ個の点を結んで線を描く
  int   i;
  glBegin(GL_LINE_STRIP);
  for( i=0; i<n; i++ )glVertex3fv(x[i]);
  glEnd();
}

void myinit(void)    // 初期化
{
  float axis[][3]={1.,0.,0., 0.,0.,0., 0.,1.,0., 0.,0.,0., 0.,0.,1.};
  float charX[][3]={1.2,0.2,0.15, 1.2,-0.2,-0.15, 1.2,0.2,-0.15,
                   1.2,-0.2,0.15};
  float charY[][3]={-0.2,1.5,0., 0.,1.3,0., 0.,1.1,0., 0.,1.3,0., 0.2,1.5,0.};
  float charZ[][3]={-0.15,0.2,1.1, 0.15,0.2,1.1, -0.15,-0.2,1.1, 0.15,-0.2,1.1};

  glClearColor(1.0, 1.0, 1.0, 0.0);
  glEnable(GL_DEPTH_TEST);// 奥行き情報で陰線処理をする
  glDisable(GL_LIGHTING);

  glNewList(AXIS, GL_COMPILE);
  glColor3f( 0.,0.,0. );// Black
  glLineWidth(1.);// 線幅１pixel
  drawLines( axis,5 );// 座標軸を描く
  drawLines( charX,2 );// X字
  drawLines( &charX[2],2 );
  drawLines( charY,3 );// Y字
  drawLines( &charY[3],2 );
  drawLines( charZ,4 );// Z字
  glEndList();
}

void nurbsinit()
{
  GLfloat mat_diffuse[] = { 0.7, 0.7, 0.7, 1.0 };
  GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
  GLfloat mat_shininess[] = { 100.0 };

  glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
  glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

  //glEnable(GL_LIGHTING);
  glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
  glEnable(GL_LIGHT0);
  glDepthFunc(GL_LESS);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_AUTO_NORMAL);
  glEnable(GL_NORMALIZE);

  myNurbs = gluNewNurbsRenderer(); // NURBS曲面を表示するオブジェクトを準備する
  gluNurbsProperty(myNurbs, GLU_SAMPLING_TOLERANCE, 25.0); // 誤差を25ピクセル以内のポリゴン表示
}

void mousevalue( int x, int y, float *xw, float *yw )
{ // マウス座標を正規化座標系(-1 < x,y < 1)の値に変換　
  *xw = (2.0f*(float)x/(float)_w - 1.0f)*_aratio;
  *yw = 2.0f*(float)( _h-y-1)/(float)_h - 1.0f;
}

void mouse_interrupt(int butn, int state, int x, int y)
{
  if( state == GLUT_DOWN ){// ボタン押された時
    switch( butn ){
    case GLUT_LEFT_BUTTON: // 左ボタン押された
      mxcopy( mxsave, mx ); // 変換行列をmxにコピーしてドラッグに備える
      mousevalue( x, y, &mx0, &my0 );
      break;
    case GLUT_RIGHT_BUTTON:
    default: break;
    }
    glutPostRedisplay(); // 再描画
  }
  else {// ボタンが放された時
    mxcopy( mx, mxsave );
  }
}

void motion(int x, int y)
{// マウスドラッグによって回転変換行列を更新する
  float wrk[16];
  mousevalue( x, y, &mx1, &my1 );
  if( trackball(wrk, mx1, my1, mx0, my0) ){
    mxcopy( mxsave, mx );
    mxmultiply( mx, wrk );
    glutPostRedisplay();
  }
}

void keyboard(unsigned char key, int x, int y)
{
  switch (key) {
  case 27: case 13: case 10: exit(0);  // ESCキーまたはENTERキーで終了
  case ' ': dispNurbs=1-dispNurbs; break;// スペースキーで曲面表示
  case 'c': dispCtrlPolygon = 1 - dispCtrlPolygon; break;// 制御ポリゴン表示
  case 'a': dispAxis = 1 - dispAxis; break; // 座標軸表示
  default:break;
  }
  glutPostRedisplay();
}

void  reshape(int w, int h)
{   // ウィンドウを再描画
  h = (h == 0) ? 1 : h;
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  _w = w; _h = h;
  _aratio = (float)w/(float)h;
  gluPerspective( 40., _aratio, 50., 1000. );// 透視変換
  glMatrixMode(GL_MODELVIEW);
}

int main(int argc, char** argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowSize(800, 600);
  glutInitWindowPosition(10, 10);
  glutCreateWindow(argv[0]);
  myinit();
  nurbsinit();
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutKeyboardFunc(keyboard);
  glutMotionFunc(motion);
  glutMouseFunc(mouse_interrupt);
  glutMainLoop();
  return 0;
}
