/*
 マウスクリックによって、点を生成できる
 点列を制御点列としてBezier曲線を、関数 drawBezier() の中につくり
 表示させること
 制御点はマウスドラッグで位置の変更が可能、点の消去はマウス右クリックで指定
 点を指定しないで、右クリックだと、最初に入力した点が消去される。
 次数を変更できるように作る場合、数字キーによって変更される（1~9次まで）
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#define MAX 100
#define NEAR 8        // 近傍の範囲

float pt[MAX][2];     // マウスからの点の座標を格納
int np = 0;           // マウスからの点の個数
int pickupPoint = -1; // マウスで指定した点番号
int degree = 2;       // 自由度
int pointsize = 2;

// (x,y)位置に文字列stringを表示する
void strout(int x, int y, char *string) {  
  void *font = GLUT_BITMAP_TIMES_ROMAN_24;
  int len, i;
  glRasterPos2i(x, y);
  len = (int) strlen(string);
  for (i = 0; i < len; i++) {
    glutBitmapCharacter(font, string[i]);
  }
}

float bernstein(int degree, float index, float t) {
  if (degree == 0) {
    if (index == 0) return 1;
    else            return 0;    
  }
  return (10-t) * bernstein(degree-1, index, t) + t * bernstein(degree-1, index-1, t);  
}
// このdrawBezier()関数の中を書き換えて、Bezier曲線を描くようにすること
// 次数を変えられるBezier関数を描くときには、グローバル変数degreeを使えば良い
void drawBezier(void) {
  int i, k;
  float t;
  glColor3f(1.,0.,0.8);
  glLineWidth(3.);

  glPointSize(pointsize);
  glBegin(GL_POINTS);
  for (i = degree; i < np; i += degree) {
    for (t = 0.0; t < 10.0; t += 0.005) {
      float x = 0, y = 0;
      for (int j = 0; j <= degree; j++) {        
        if (i/degree > 1)
          k = i / (i / degree);
        else
          k = i;
        float tmp = bernstein(degree, k-j, t);
        x += pt[i-j][0] * tmp;
        y += pt[i-j][1] * tmp;
      }
      x /= pow(10, degree);
      y /= pow(10, degree);
      glVertex2f(x, y);
    }
  }
  glEnd();
}


void display(void) {
  int i;
  char s[64],s1[64];

  glClear(GL_COLOR_BUFFER_BIT);

  glColor3f(0.2, 0.8, 0.);
  glPointSize(7.);     // 点の大きさ（ピクセル単位）
  glBegin(GL_POINTS);  // 制御点を表示
  for (i = 0; i < np; i++)
    glVertex2fv(pt[i]);
  glEnd();

  glLineWidth(1.);     // 線の太さ（ピクセル単位）
  glBegin(GL_LINE_STRIP);  // 制御点を結んだ制御多角形を表示
  for (i = 0; i < np; i++)
    glVertex2fv(pt[i]);
  glEnd();

  drawBezier();  // ベジェ曲線を描く

  glLineWidth(2.);
  strcpy(s,"Bezier Curve: degree="); // "で囲まれた文字列をsに入れる
  sprintf(s1,"%d",degree);            // 整数degreeの値を文字列s1に変換
  strcat(s, s1);                      // 文字列s1を文字列ｓの後ろに加える
  strcat(s, "(NUMKEY)");              // "で囲まれた文字列をsの後ろに加える
  glColor3f(0.0, 0.0, 0.0);
  strout(10, 30, s);                  // (10, 30)位置に文字列sを表示
  glutSwapBuffers();
}

// マウス位置の近傍に登録された点があればその番号を返す
//  無ければ-1を返す
int found(int x, int y)  {  
  int i, dx, dy;
  for(i = 0; i < np; i++) {
    dx = abs(x - pt[i][0]);
    dy = abs(y - pt[i][1]);
    if(dx + dy > NEAR)
      continue;
    return i;
  }
  return -1;
}

// 既に登録された点がマウスカーソルの近傍に無ければ、新しい点としてマウスクリックした点を登録する
// 近傍に点があれば、その点をドラッグして位置を変更できる
void mouseClick(int button, int state, int x, int y) {  
  int i, k;
  switch (button) {
  case GLUT_LEFT_BUTTON:      // マウス左ボタンが
    if (state == GLUT_DOWN){  // 押された場合（新規登録、変更）
      if ((pickupPoint=found(x,y)) >= 0)
    return;                   // 登録点が指定された
      if (np >= MAX-1)
    break;
      pt[np][0] = (float)x;
      pt[np][1] = (float)y;
      np++;                   // 点の登録
    } else if (state == GLUT_UP) {
      if (pickupPoint >= 0) {
    pt[pickupPoint][0] = (float)x;
    pt[pickupPoint][1] = (float)y;  // 点の位置変更
      }
    }
    break;
  case GLUT_RIGHT_BUTTON:  // 右ボタンの場合（点の消去）
    if (state == GLUT_DOWN) {
      if (np > 0 && (k = found(x, y)) >= 0) {
    for (i = k; i < np - 1; i++) {  // カーソル位置の点を消去
      pt[i][0] = pt[i+1][0];
      pt[i][1] = pt[i+1][1];
    }
    np--;
      }
      else
    np--;  // カーソル位置になければ最後の点を消去
    }
    break;
  default: break;
  }
  glutPostRedisplay();
}

void motion(int x, int y) {  // マウスドラッグするときの処理
  if(pickupPoint >= 0) {
    pt[pickupPoint][0] = (float)x;
    pt[pickupPoint][1] = (float)y;  // 特定の点を動かす
    glutPostRedisplay();
  }
}

void keyboard(unsigned char key, int x, int y) {
  switch (key) {
  case '1':degree=1; break;
  case '2':degree=2; break;
  case '3':degree=3; break;
  case '4':degree=4; break;
  case '5':degree=5; break;
  case '6':degree=6; break;
  case '7':degree=7; break;
  case '8':degree=8; break;
  case '9':degree=9; break;
  case 27: case 13: case 10: exit(0);
  default: return;
  }
  glutPostRedisplay();
}

void reshape(int w, int h) {
  h = (h == 0) ? 1 : h;
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0., (float)w, (float)h, 0., -10., 10.);
  glMatrixMode(GL_MODELVIEW);
}

int main(int argc, char** argv) {
  glutInitWindowSize(800, 600);
  glutInitWindowPosition(10, 10);
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
  glutCreateWindow(argv[0]);
  glClearColor(1., 1., 1., 1.);
  glColor3f(0., 0., 0.);
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutMouseFunc(mouseClick);
  glutMotionFunc(motion);  // マウスドラッグ
  glutKeyboardFunc(keyboard);
  glutMainLoop();
  return 0;
}
