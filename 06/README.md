# CG基礎 第6回目 モデリング
今回から担当教員が変わりました.

使用言語がCからC++となり阿鼻叫喚な予感ですが教員が配布してくれるソースコードは前よりマシなので人権を得られそうです.

## OBJView
MacとWindowsで動作するらしい.ここにあるソースコードはMac版でLinuxでも多分動作する.(未確認) Windowsなにそれ美味しいの?

### (2) 立方体モデルデータ
![](img/square.png)

### (4) 三角形の集合によるパラメトリック曲面
![](img/parametoric_surface.png)

### (5) 曲面モデル

* (a) 波紋

![](img/ripple.png)

* (b) ガウス関数

![](img/gauss_func.png)

* (c) 球

![](img/sphere.png)

