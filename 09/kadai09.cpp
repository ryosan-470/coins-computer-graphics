#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <iostream>

// 3次元ベクトル
class Vector3d {
public:
  double x, y, z;
  Vector3d() { x = y = z = 0; }
  Vector3d(double _x, double _y, double _z) { x = _x; y = _y; z = _z; }
  void set(double _x, double _y, double _z) { x = _x; y = _y; z = _z; }

  // 長さを1に正規化する
  void normalize() {
    double len = length();
    x /= len; y /= len; z /= len;
  }

  // 長さを返す
  double length() { return sqrt(x * x + y * y + z * z); }

  // s倍する
  void scale(const double s) { x *= s; y *= s; z *= s;}

  // 代入演算の定義
  Vector3d& operator = (const Vector3d& v){ x = v.x; y = v.y; z = v.z; return *this; }

  // 加算代入の定義
  Vector3d& operator+= (const Vector3d& v) { x += v.x; y += v.y; z += v.z; return( *this ); }

  // 減算代入の定義
  Vector3d& operator-= (const Vector3d& v) { x -= v.x; y -= v.y; z -= v.z; return( *this ); }

  void debugout() {
   std:: cout << "Vec(" << x << " " << y << " " << z << ")" << std::endl;
  }
};

Vector3d operator+( const Vector3d& v1, const Vector3d& v2 ) { return( Vector3d( v1.x+v2.x, v1.y+v2.y, v1.z+v2.z ) );}
Vector3d operator-( const Vector3d& v1, const Vector3d& v2 ) { return( Vector3d( v1.x-v2.x, v1.y-v2.y, v1.z-v2.z ) );}
Vector3d operator-( const Vector3d& v ) { return( Vector3d( -v.x, -v.y, -v.z ) ); }
Vector3d operator*( const double& k, const Vector3d& v ) { return( Vector3d( k*v.x, k*v.y, k*v.z ) );}
Vector3d operator*( const Vector3d& v, const double& k ) { return( Vector3d( v.x*k, v.y*k, v.z*k ) );}
Vector3d operator/( const Vector3d& v, const double& k ) { return( Vector3d( v.x/k, v.y/k, v.z/k ) );}

//----  内積の定義
double operator*( const Vector3d& v1, const Vector3d& v2 ) { return( v1.x*v2.x + v1.y*v2.y + v1.z*v2.z );}
//----  外積の定義
Vector3d  operator%( const Vector3d& v1, const Vector3d& v2 ) { return( Vector3d( v1.y*v2.z - v1.z*v2.y,  v1.z*v2.x - v1.x*v2.z,  v1.x*v2.y - v1.y*v2.x ) );}


// ボールの状態を表すクラス
class Ball {
public:
  Vector3d a; // 加速度ベクトル
  Vector3d v; // 速度ベクトル
  Vector3d p; // 位置
  double r;   // 半径

  Ball() {  // 初期状態の設定
    r = 1; 
    a.set(0, 0, 0);     // 初期化速度の指定
    // v.set(0, 0, 0);     // 初速度の指定
    v.set(0.01, 0, 0);
    p.set(0, 10, 0);    // 初期位置の指定
  } 
};

GLfloat ball_color[] = { 0.8, 0.2, 0.2, 1.0 };
GLfloat floor_color[] = { 0.2, 0.2, 1.0, 1.0 };

double rotateAngleH_deg;
double rotateAngleV_deg;
int preMousePositionX;
int preMousePositionY;

Ball ball;
bool bRunning;
bool bReset = false;
#define FLOOR_SIZE 10 // 床の縦横の寸法をここで指定している

void drawBall(void) {
  glPushMatrix();
  glTranslated(ball.p.x, ball.p.y, ball.p.z);
  glMaterialfv(GL_FRONT, GL_DIFFUSE, ball_color);
  glutSolidSphere(ball.r, 32, 32);
  glPopMatrix();
}

void drawFloor(void) {
  glPushMatrix();
  glTranslated(0, 0, 0);

  glScaled(FLOOR_SIZE, 0.2, FLOOR_SIZE); 
  glMaterialfv(GL_FRONT, GL_DIFFUSE, floor_color);
  glutSolidCube(1.0f);
  glPopMatrix();
}

void display(void) {
  // 画面クリア
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // モデルビュー変換行列の初期化 
  glLoadIdentity();

  // 視点の移動 
  glTranslated(0, 0.0, -50);
  glRotated(rotateAngleV_deg, 1.0, 0.0, 0.0);
  glRotated(rotateAngleH_deg, 0.0, 1.0, 0.0);

  // 光源の位置を設定 
  static GLfloat lightpos[] = { 3.0, 4.0, 5.0, 0.0 }; 
  glLightfv(GL_LIGHT0, GL_POSITION, lightpos);

  // シーンの描画
  drawFloor();
  drawBall();

  // 画面の更新
  glFlush();
}

void resize(int w, int h) {
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(30.0, (double)w / (double)h, 1.0, 100.0);
  glMatrixMode(GL_MODELVIEW);
}

void keyboard(unsigned char key, int x, int y) {
  // ESC または q をタイプしたら終了
  if (key == '\033' || key == 'q') { exit(0);}

  // a でアニメーションのオンオフ
  if (key == 'a') { bRunning = !bRunning; }
  // r でアニメーション初期位置へ
  if (key == 'r') { bReset = true;}
}

void mouse(int button, int state, int x, int y) {
  switch (button) {
  case GLUT_LEFT_BUTTON:
    preMousePositionX = x;
    preMousePositionY = y;
    break;
  case GLUT_MIDDLE_BUTTON:
    break;
  case GLUT_RIGHT_BUTTON:
    break;
  default:
    break;
  }
}

// =======================================================
//  一定時間ごとに呼ばれる。
//  ここで、対象物の物理量（力、加速度、速度、位置）を更新する
// =======================================================
void timer(int value) {
	
  if(bRunning) { // シミュレーション実行中の処理

    /* 加速度(ball.a)を設定する処理を入れる */
    ball.a = Vector3d(0, -0.01, 0);
    /* 加速度に基づいて、速度(ball.v)を更新する処理を入れる(単純に現在の速度に加速度を加算すればよい)*/
    ball.v += ball.a;
    ball.p += ball.v; // 位置の更新（単純に速度を加算すればよい。ベクトルの加算）

    if (ball.p.y < ball.r
		&& ball.p.x < (FLOOR_SIZE + ball.r) / 2 && ball.p.x > -((FLOOR_SIZE + ball.r / 2))
		&& ball.p.z < (FLOOR_SIZE + ball.r) / 2 && ball.p.z > -((FLOOR_SIZE + ball.r / 2))) {
      ball.p.y = ball.r;
      ball.v.y = - 0.8 * ball.v.y;
    }
  }

  if (bReset) {
    ball = Ball();
    bRunning = false;
    bReset = false;
  }
  glutPostRedisplay(); // 描画内容の更新
  glutTimerFunc(10 , timer , 0); // 一定時間経過後に、この関数を再実行する
}

void motion(int x, int y) {
  int diffX = x - preMousePositionX;
  int diffY = y - preMousePositionY;

  rotateAngleH_deg += diffX * 0.1;
  rotateAngleV_deg += diffY * 0.1;

  preMousePositionX = x;
  preMousePositionY = y;

  glutPostRedisplay();
}


void init(void) {
  glClearColor(1.0, 1.0, 1.0, 0.0);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
}

int main(int argc, char *argv[]) {
  glutInit(&argc, argv);
  glutInitWindowSize(600,600);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH);
  glutCreateWindow(argv[0]);
  glutDisplayFunc(display);
  glutReshapeFunc(resize);
  glutKeyboardFunc(keyboard);
  glutMouseFunc(mouse);
  glutMotionFunc(motion);
  glutTimerFunc(0 , timer , 0);

  bRunning = true;
  init();
  glutMainLoop();
  return 0;
}
