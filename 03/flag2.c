/*
 * スペースキー:回転停止/開始
 * ESC or Enter:プログラム終了
 */
#define BAR    1
#define COLUMN 2
#define FLAG   3
#define PIN    4
#define AR     0.017453 // pi/180.0 度からラジアンへ

#include <math.h>
#include <stdlib.h>
#include <GLUT/glut.h>

float rote = 0.0, size = 10.0;
int move = 1;
int time1 = 10; // 10 msec

void display(void) {
  glClear(GL_COLOR_BUFFER_BIT);
  glPushMatrix();
  rote += 1.0;
  if (rote > 360.0)
    rote -= 360.0;
  glScalef(size, size, 1.0);
  glColor3f(1.0, 0.7, 0.3);
  glCallList(COLUMN); // 支柱

  glRotatef(rote, 0.0, 0.0, 1.0);
  glColor3f(0.7, 1.0, 0.1);
  glCallList(BAR);     // 棒
  glCallList(PIN);

  // 旗1
  glPushMatrix();
    glTranslatef(-10.0, 0.0, 0.0);    // 移動 
    glRotatef(-rote, 0.0, 0.0, 1.0);
    glPushMatrix();
       glRotatef(45.0, 0.0, 0.0, 1.0); // 45度回転
       glTranslatef(-2.0, -2.0, 0.0);
       glColor3f(0.0, 0.7, 1.0);
       glCallList(FLAG);               // FLAGを表示
       glTranslatef(2.0, -1.0, 0.0);
    glPopMatrix();
    glCallList(PIN);
  glPopMatrix();

  // 旗2
  glPushMatrix();
    glTranslatef(10, 0.0, 0.0);
    glRotatef(-rote, 0.0, 0.0, 1.0);
    glPushMatrix();
      glRotatef(-45.0, 0.0, 0.0, 1.0);
      glTranslatef(2.0, -2.0, 0.0);
      glColor3f(0.0, 0.7, 1.0);
      glCallList(FLAG);
      //      glTranslatef(GLfloat x, GLfloat y, GLfloat z)
    glPopMatrix();

    glCallList(PIN);
  glPopMatrix();

  glPopMatrix();
  glutSwapBuffers();
}

void myinit(void) {
  glClearColor(1.0, 1.0, 1.0, 1.0);

  glNewList(COLUMN, GL_COMPILE); // 支柱
    glBegin(GL_TRIANGLE_STRIP);  // 三角形の連なりで台形を作成
      glColor3f(1.0, 0.7, 0.3);
      glVertex2i(-1, 1);
      glVertex2i(-3, -15);
      glVertex2i(1, 1);
      glVertex2i(3, -15);
    glEnd();
  glEndList();

  glNewList(BAR, GL_COMPILE);  // 棒
  glBegin(GL_TRIANGLE_STRIP);  // 三角形の連なりで長方形を作成
  glVertex2f(-10, 0.5); glVertex2f(-10, -0.5);
  glVertex2f(10, 0.5); glVertex2f(10, -0.5);
  glEnd();
  glEndList();

  glNewList(FLAG, GL_COMPILE); // 旗
  glBegin(GL_TRIANGLE_STRIP);  // 三角形の連なりで正方形を作成
  glVertex2i(-2, 2); glVertex2i(-2, -2);
  glVertex2i(2, 2);  glVertex2i(2, -2);
  glEnd();
  glEndList();

  glNewList(PIN, GL_COMPILE); // 回転中心軸
  glColor3f(0.0, 0.0, 0.0);
  glBegin(GL_TRIANGLE_FAN);
  glVertex2i(0, 0);
  int i;
  for (i=0; i<=360; i+=30)
    glVertex2f(0.4*cos(AR*(double)i), 0.4*sin(AR*(double)i));
  glEnd();
  glEndList();
}

// time ミリ秒ごとに割り込み
void timer(int value) {
  if (move == 1)
    glutPostRedisplay();
  glutTimerFunc(time1, timer, 0);
}

void keyinput(unsigned char key, int x, int y) {
  switch(key) {
  case 27:
  case 10:
  case 13:
    exit(EXIT_SUCCESS);
    break;
  case ' ':
    move = 1 - move;
    break;
  default:
    break;
  }
  glutPostRedisplay();
}

void reshape(int w, int h) {
  h = (h == 0) ? 1 : h;
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-w/2.0, w/2.0, -h/2.0, h/2.0, -h/2.0, h/2.0);
  if (w > h)
    size = (float)h/40.0;
  else
    size = (float)w/40.0;
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

int main(int argc, char **argv) {
  glutInitWindowSize(600, 600);
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow(argv[0]);
  myinit();
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutKeyboardFunc(keyinput);
  glutTimerFunc(time1, timer, 0);
  glutMainLoop();
  return EXIT_SUCCESS;
}
