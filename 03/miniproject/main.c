#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#if __linux__
#include <GL/glut.h>
#elif __APPLE__
#include <GLUT/glut.h>
#endif
// bool 代数
#define FALSE 0
#define TRUE  1

#define BALL   2
#define RACKET 3
#define AR     0.017453 // pi/180.0 度からラジアンへ
#define WIDTH  800
#define HEIGHT 600

float rote = 0.0, size = 2.0;
float vx = 0., vy = 0.;
int move = 1;
int time1 = 10; // 10 msec
int xflag = TRUE; // 正の方向に動かす
int yflag = TRUE;

float speed = 4;
char *ptr, sp[512];
void strout(float x, float y, char *str) {
  void *font = GLUT_BITMAP_TIMES_ROMAN_24;
  glRasterPos2f(x, y);
  while (*str)
    glutBitmapCharacter(font, *str++);
}

void display(void) {
  glClear(GL_COLOR_BUFFER_BIT);
  // 描画制限値
  float pxlim = (WIDTH / 2) / size; 
  float mxlim = -pxlim;
  float pylim = (HEIGHT / 2) / size;
  float mylim = - pylim;
  if (speed < 0)
    speed = 0;
  if (vx > pxlim) {
    vx = pxlim; xflag = FALSE;
  }
  if (vx < mxlim) {
    vx = mxlim; xflag = TRUE;
  }
  if (vy > pylim) {
    vy = pylim; yflag = FALSE;
  }
  if (vy < mylim) {
    vy = mylim; yflag = TRUE;
  }
  else {
    if (xflag == TRUE)  
      vx += speed;
    if (xflag == FALSE)
      vx -= speed;
    if (yflag == TRUE)
      vy += speed;
    if (yflag == FALSE)
      vy -= speed;
  }
  glPushMatrix();
  glColor3f(0.0, 0.7, 1.0);
  glScalef(size, size, 0);  // 拡大縮小
  glTranslatef(vx, vy, 0);  // 移動する
  glCallList(BALL);
  glPopMatrix();

  glPushMatrix();
    glScalef(size, size, 0);  // 拡大縮小
    glTranslatef(-vx, vy, 0);  // 移動する
    glCallList(BALL);
  glPopMatrix();

  
  glPushMatrix();
  glTranslatef(0.0, 0.0, 0.0);
  sprintf(sp, "%d", (int)speed);
  ptr = sp;
  strout(mxlim - 150, pylim + 100, ptr);
  
  glPopMatrix();
  glutSwapBuffers();
}

void myinit(void) {
  glClearColor(1.0, 1.0, 1.0, 1.0);
  /* 球の定義 */
  glNewList(BALL, GL_COMPILE); 
  glBegin(GL_TRIANGLE_STRIP);
  glVertex2i(-2, 2); glVertex2i(-2, -2);
  glVertex2i(2, 2);  glVertex2i(2, -2);
  glEnd();
  glEndList();
}

// time ミリ秒ごとに割り込み
void timer(int value) {
  if (move == 1)
    glutPostRedisplay();
  glutTimerFunc(time1, timer, 0);
}

void keyinput(unsigned char key, int x, int y) {
  switch(key) {
  case 27:
  case 10:
  case 13:
    exit(EXIT_SUCCESS);
    break;
  case ' ':
    move = 1 - move;
    break;
  default:
    break;
  }
  glutPostRedisplay();
}

void special_keyinput(int key, int x, int y) {
  switch(key) {
  case GLUT_KEY_LEFT:
    if (xflag == TRUE) xflag = FALSE;
    else xflag = TRUE;
    break;
  case GLUT_KEY_RIGHT:
    if (yflag == TRUE) yflag = FALSE;
    else yflag = TRUE;
    break;
  case GLUT_KEY_UP:
    speed += 1; break;
  case GLUT_KEY_DOWN:
    speed -= 1; break;
  default:
    break;
  }
  glutPostRedisplay();
}
void reshape(int w, int h) {
  h = (h == 0) ? 1 : h;
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-w/2.0, w/2.0, -h/2.0, h/2.0, -h/2.0, h/2.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

int main(int argc, char **argv) {
  glutInitWindowSize(WIDTH, HEIGHT);
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow(argv[0]);
  myinit();
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutSpecialFunc(special_keyinput);
  glutKeyboardFunc(keyinput);
  glutTimerFunc(time1, timer, 0);
  glutMainLoop();
  return EXIT_SUCCESS;
}
