// ４つの三角形から正四面体を折り曲げてアニメーションをつくる例
// 折り曲げる操作の変換行列と、描くための変換行列とは独立に
//  考えてプログラムを作っている
//  F（Folding)キーで折り曲げ、U（Unfolding）キーで展開する
// 矢印キーを押し続けることで物体を回転させる

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#ifdef __linux__
#include <GL/glut.h>
#elif __APPLE__
#include <GLUT/glut.h>
#endif
#define TRIANGLE  1

// define 文で　演算を定義する場合は、必ず(　)で全体を囲むこと（意図しない計算になる可能性を消すため）
#define D    (sqrt(3.)/6.)  // 三角形の中心から辺までの距離 = 0.288675.....
#define D2   (sqrt(3.)/3.)  // 三角形の中心から頂点までの距離 = 0.57735....

float xrot=0., yrot=0.,rotangle=0.; // 全体の回転角と折り曲げ角

void display(void)
  {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 画面と奥行き情報を初期化

    // 表示物を回転させて見る方向を変える
    glLoadIdentity();        // 単位行列をモデルリング視野変換行列にセットする
    glRotatef( xrot,1.,0.,0. ); // (1,0,0)軸周りにxrot度の角度の回転変換行列を掛ける（視野変換用）
    glRotatef( yrot,0.,1.,0. ); // (0,1,0)軸周りにyrot度の角度の回転変換行列を掛ける（視野変換用）
    glScalef( 15.,15.,15. );    // 15倍の拡大変換行列を掛ける（視野変換用）

    glCallList( TRIANGLE );         // 折り曲げ操作で動かない固定した三角形を描く

    // 最初の折り曲げ部（以下の3行）
    glTranslatef( 0.,D2,0. );            // 2行下での平行移動を元に戻す
    glRotatef( rotangle,0.5,-0.866,0. ); // 最初の折り曲げ
    glTranslatef( 0.,-D2,0. );           // 折る線上に原点が来るように移動
    // 2番目の三角形の表示部（以下の5行）
    glPushMatrix();                 // 最初の折り曲げ部を含んだ変換行列を保存
    glTranslatef( 0.5,D,0. );     // 最初の固定三角形に接する位置に平行移動
    glRotatef( 180.,0.,0.,1. );   // 上下逆にするために180度の回転変換
    glCallList( TRIANGLE );       // 固定した三角形の右隣りの三角形を描く
    glPopMatrix();                  // 最初の折り曲げが有効

    // 最初の折り曲げの状態で、2番目の折り曲げ部（以下の3行）
    glTranslatef( 0.5,-D,0. );      // 2行下での平行移動を元に戻す
    glRotatef( rotangle,-0.5,-0.866,0. ); // 2回目の折り曲げ
    glTranslatef( -0.5,D,0. );      // 2回目の折る線上に原点が来るように平行移動
    // 3番目の三角形の表示部（以下の4行）
    glPushMatrix();                 // 1,2回目の折り曲げ操作が有効な状態を保存
    glTranslatef( 1.0,0.,0. );    // 3番目の三角形が2番めの三角形に接する位置に平行移動
    glCallList( TRIANGLE );       // 3番目の三角形を描く
    glPopMatrix();                  // 1,2回目の折り曲げが有効な変換行列


    // 2つの折り曲げ部を含んだ状態で、3番目の折り曲げ部（以下の3行）
    glTranslatef( 1.5,-D,0. );           // 2行下での平行移動を元に戻す
    glRotatef( rotangle,0.5,-0.866,0. ); // 3回目の折り曲げ操作
    glTranslatef( -1.5,D,0. );           // 3回目の折る線上に原点が来るように平行移動

    // 4番目の三角形の表示部（これ以降が無いので、glPushMatrix( ), glPopMatrix( )を使う必要がない）
    glTranslatef( 1.5,D,0. );       // 3番目の三角形に接する位置に平行移動
    glRotatef( 180.,0.,0.,1. );     // 上下を逆にする
    glCallList( TRIANGLE );         // 最後の三角形を描く

    glutSwapBuffers();              // 表示用の画面領域と書きこみ用画面領域を交換して表示する
  }

void myinit(void) // OPENGL等　初期化
  {
    int i;
    float light_pos0[] = { -5., 10.0, 50.0, 0. }; // 光源位置
    float ambient[]={0.5,0.5,0.5,1.};             // 環境光の色と強さ
    float color0[] = {0.7, 0.7, 0.7, 1.};         // 灰色
    float color1[] = {0., 1., 0., 1.};            // 緑色
    float triangle[][3] ={ 0.,D2,0., 0.5,-D,0., -0.5,-D,0.}; // 中心が原点の三角形
    float norm[]={0.,0.,-1.};                     // 面の外向きの法線方向

    glLightModeli( GL_LIGHT_MODEL_TWO_SIDE,GL_TRUE ); // 表裏両面を照らす
    glLightfv(GL_LIGHT0, GL_POSITION, light_pos0);    // 光源位置を指定
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);        // 環境光を指定
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // 背景色で初期化、奥行き情報初期化
    glClearColor(1.,1.,1.,0.);   // 消去色（背景色）
    glEnable(GL_DEPTH_TEST);     // 奥行き深さ情報を更新する
    glEnable(GL_NORMALIZE);      // 面を描く時に面の法線データを正規化する
    glEnable(GL_LIGHTING);       // 光源による照明を有効化する
    glEnable(GL_LIGHT0);         // 0番目の光源を有効にする
    glMaterialfv( GL_BACK, GL_DIFFUSE, color0 );// 裏面色
    glMaterialfv( GL_FRONT, GL_DIFFUSE, color1 );//　表面色

    glNewList( TRIANGLE, GL_COMPILE ); // 表示リストを作成
    glBegin(GL_TRIANGLES);   // 三角形定義開始
    glNormal3fv( norm );   // 以降の頂点の法線方向を指定する
    for( i=0; i<3; i++ )glVertex3fv(triangle[i]); // 三角形頂点を指定
    glEnd();                 // 三角形の定義終了
    glEndList();               // 表示リスト作成終了
  }

void reshape(int w, int h)        // 再描画関数
  {
    h = (h == 0) ? 1 : h;           //
    glViewport(0, 0, w, h);   // 画面の表示領域を指定（全画面を使用可にする）
    glMatrixMode(GL_PROJECTION);  // 以降は投影変換行列を操作する
    glLoadIdentity();             // 単位行列を設定
    gluPerspective(60.0, (GLfloat) w/(GLfloat) h, 10.0, 100.0); // 透視投影変換行列を掛ける
    glTranslatef( 0.,0.,-50. );   // 平行移動行列を掛ける
    glMatrixMode(GL_MODELVIEW);   // 以降はモデリング視野変換行列を操作する
  }

void special_input(int key, int x, int y)
  {
    switch(key){
    case GLUT_KEY_LEFT:  // 左矢印キーが押されたとき
      yrot -= 2.; if( yrot<0. ) y += 360.; break;
    case GLUT_KEY_RIGHT: // 右矢印キーが押されたとき
      yrot += 2.; if( yrot>360. ) y-= 360.; break;
    case GLUT_KEY_UP:    // 上矢印キー
      xrot -= 2.; if( xrot <0. ) x += 360.; break;
    case GLUT_KEY_DOWN:  // 下矢印キー
      xrot += 2.; if( xrot > 360. ) x-=360.; break;
       }
    glutPostRedisplay();   // 再描画
  }

void key_input(unsigned char key, int x, int y)
  {
    switch(key){
    case 'f': rotangle +=1.; // Fキーが押された時に折り曲げる
      if( rotangle>109.4714 ) rotangle=109.4714;
      glutPostRedisplay(); break;
    case 'u': rotangle -=1.; // Uキーが押された時に展開する
      if( rotangle<0. ) rotangle=0.;
      glutPostRedisplay(); break;
    case 27:            // Escape キー
    case 13:            // CarriageReturn キー
    case 10:   exit(0); // LineFeed キー
    default:   return;
       }
  }

int main(int argc, char **argv)
  {
    glutInit(&argc, argv);
    // ダブルバッファ、画素毎に色指定、デプスバッファを使うモードを指定
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize (800, 800);
    glutCreateWindow(argv[0]);
    myinit();
    glutDisplayFunc(display); // 表示関数を指定
    glutReshapeFunc(reshape); // 再描画関数を指定
    glutMouseFunc(NULL);      // マウスは使わない
    glutSpecialFunc(special_input); // 矢印キーの割込み処理関数を指定
    glutKeyboardFunc(key_input);  // キーボード割込み処理関数を指定
    glutPostRedisplay();
    glutMainLoop();
    return 0;
  }
