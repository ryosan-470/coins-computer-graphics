#include <GLUT/glut.h>
#include <math.h>
#include <stdlib.h>

/* 画面の位置(x,y)に点を描く */
void ploti(int x, int y) {
  glBegin(GL_POINTS);
  glVertex2i(x, y);
  glEnd();
}

/* 始点(x1, y1)から終点(x2, y2)まで線を描画する */
void line(int x1, int y1, int x2, int y2) {
  // 初期化
  int x = x1, y = y1;
  ploti(x, y);

  int dx = x2 - x1;
  int dy = y2 - y1;
  int e = -dx;

  while (x < x2) {
    x++;
    e = e + 2*dy;
    if (e > 0) {
      y++; e = e - 2*dx;
    }
    if (dy >= 0 && dx >= dy)
      ploti(x, y);
  }
}

/* 半径r */
void octant(int r) {
  int x = 0, y = r, d = 3 - 2*x;
  while (x < y) {
    if (d < 0)
      d = d + 4 * x + 6;
    else {
      d = d + 4 * (x - y) + 10;
      y = y - 1;
    }
    x++;
    ploti(x, y);
  }
  
}

void display(void) {
  int i;
  double t, m = 400, dr = 0.017453; // dr = \pi/180.0 からラジアンへ
  
  glClear(GL_COLOR_BUFFER_BIT);
  for(i=0; i <= 45; i+=5) {
    t = (double)i*dr;
    line(0, 0, (int)(m*cos(t)), (int)(m*sin(t)));
  }
  for(i=20; i<=400; i+=20)
    octant(i);
  glFlush();
}

void myinit(void) {
  glClearColor(1.0, 1.0, 1.0, 0.0); // 背景色
  glColor3f(0.0, 0.0, 0.0);         // 描画色
}

void reshape(int w, int h) {
  h = (h == 0) ? 1 : h;
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-100, (float)w-101.0, -100.0, (float)h-101.0, -1.0, 1.0);
  glMatrixMode(GL_MODELVIEW);
}

/* ESCキーやEnterキーが入力されたらプログラムを終了する */
void keyboard(unsigned char key, int x, int y) {
  switch (key) {
  case 27: // ESC
  case 13:
  case 10:
    exit(EXIT_SUCCESS);
  default:
    break;
  }
}

int main(int argc, char **argv) {
  glutInit(&argc, argv);
  glutInitWindowSize(620, 620);
  glutInitWindowPosition(10, 10);
  glutInitDisplayMode(GLUT_RGB);
  glutCreateWindow(argv[0]);
  myinit();
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutKeyboardFunc(keyboard);
  glutMainLoop();
  return EXIT_SUCCESS;
}
