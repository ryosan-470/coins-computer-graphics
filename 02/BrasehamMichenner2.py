#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import math
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *


# 画面の位置(x,y)に点を描く
def ploti(x, y):
    glBegin(GL_POINTS)
    glVertex2i(x, y)
    glEnd()


# 始点(x1, y1)から終点(x2, y2)まで線を描画する
def linei(x1, y1, x2, y2):
    # 初期化
    mode = 0
    xstart, xend, ystart, yend = x1, x2, y1, y2

    if x1 > x2:
        mode += 1
        xstart, yend = x2, x1
    if y1 > y2:
        mode += 2
        ystart, yend = y2, y1

    if (yend - ystart > xend - xstart):
        mode += 4
        xstart, ystart = ystart, xstart
        xend, yend = yend, xend

    dx = xend - xstart
    dy = yend - ystart
    d = 2 * dy
    e = -dy
    x, y = xstart, ystart

    ploti(x, y)

    while x < xend:
        x += 1
        e += d
        if e > 0:
            y += 1
            e -= 2 * dx

        if mode == 0:
            ploti(x, y)
        elif mode == 1:
            ploti(xstart + xend - x, y)
        elif mode == 2:
            ploti(x, ystart + yend - y)
        elif mode == 3:
            ploti(xstart + xend - x, ystart + yend - y)
        elif mode == 4:
            ploti(y, x)
        elif mode == 5:
            ploti(y, xstart + xend - x)
        elif mode == 6:
            ploti(ystart + yend - y, x)
        elif mode == 7:
            ploti(ystart + yend - y, xstart + xend - x)
        else:
            break


def octant(r):
    x = 0
    y = r
    d = 3 - 2 * x

    while x < y:
        if d < 0:
            d = d + 4 * x + 6
        else:
            d = d + 4 * (x - y) + 10
            y -= 1

        x += 1
        ploti(x, y)


def display():
    r = 310
    dr = 0.017453  # dr = \pi/180.0 からラジアンへ
    glClear(GL_COLOR_BUFFER_BIT)

    for i in range(0, 16, 5):
        t = i*dr*22.5
        ix = 300*math.cos(t) + r
        iy = 300*math.sin(t) + r
        linei(r, r, ix, iy)

    circlei(r, r, 300)
    glFlush()


def init():
    glClearColor(1.0, 1.0, 1.0, 0.0)  # 背景色
    glColor3f(0.0, 0.0, 0.0)          # 描画色


def reshape(w,  h):
    if h == 0:
        h = 1

    glViewport(0, 0, w, h)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(0, w-1, 0,  h-1, -1.0, 1.0)
    glMatrixMode(GL_MODELVIEW)


def keyboard(key, x, y):
    if key == 27 or key == 13 or key == 10:
        sys.exit(0)


def main(argv):
    glutInit(argv)
    glutInitWindowSize(600, 600)
    glutInitWindowPosition(10, 10)
    glutInitDisplayMode(GLUT_RGB)
    glutCreateWindow(argv[0])
    init()
    glutDisplayFunc(display)
    glutReshapeFunc(reshape)
    glutKeyboardFunc(keyboard)
    glutMainLoop()


if __name__ == "__main__":
    main(sys.argv)
