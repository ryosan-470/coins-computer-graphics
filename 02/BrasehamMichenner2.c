#include <GLUT/glut.h>
#include <math.h>
#include <stdlib.h>

/* 画面の位置(x,y)に点を描く */
void ploti(int x, int y) {
  glBegin(GL_POINTS);
  glVertex2i(x, y);
  glEnd();
}

/* 交換用に必要な関数 */
void swap(int *a, int *b) {
  int tmp = *a;
  *a = *b;
  *b = tmp;
}

/* 始点(x1, y1)から終点(x2, y2)まで線を描画する */
void linei(int x1, int y1, int x2, int y2) {
  // Initialized
  int mode = 0;
  int xstart = x1, xend = x2;
  int ystart = y1, yend = y2;

  if (x1 > x2) {
    mode += 1;
    xstart = x2;
    xend = x1;
  }
  if (y1 > y2) {
    mode += 2;
    ystart = y2;
    yend = y1;
  }
  if (yend - ystart > xend - xstart) {
    mode += 4;
    swap(&xstart, &ystart);
    swap(&xend, &yend);
  }

  // 描画
  int dx = xend - xstart;
  int dy = yend - ystart;
  int d = 2 * dy;
  int e = -dx;
  int x = xstart;
  int y = ystart;

  ploti(x, y);

  while (x < xend) {
    x++;
    e += d;
    if (e > 0) {
      y++; e -= 2 * dx;
    }

    switch (mode) {
    case 0:
      ploti(x, y); break;
    case 1:
      ploti(xstart + xend - x, y); break;
    case 2:
      ploti(x, ystart + yend - y); break;
    case 3:
      ploti(xstart + xend - x, ystart + yend - y); break;
    case 4:
      ploti(y, x); break;
    case 5:
      ploti(y, xstart + xend - x); break;
    case 6:
      ploti(ystart + yend - y, x); break;
    case 7:
      ploti(ystart + yend - y, xstart + xend - x); break;
    default: break;
    }
  }
}

/* 中心が(x, y), 半径r の円を描画  */
void circlei(int x0, int y0, int r) {
  int x = 0, y = r;
  int d = 3 - 2 * r;

  while (x < y) {
    if (d < 0)
      d = d + 4 * x + 6;
    else {
      d = d + 4 * (x - y) + 10;
      y = y - 1;
    }
    x++;
    ploti(x0 + x, y0 + y);
    ploti(x0 + x, y0 - y);
    ploti(x0 - x, y0 + y);
    ploti(x0 - x, y0 - y);
    ploti(x0 + y, y0 + x);
    ploti(x0 + y, y0 - x);
    ploti(x0 - y, y0 + x);
    ploti(x0 - y, y0 - x);
  }
  
}

void display(void) {
  int i, ix, iy, r = 310;
  double t, dr = 0.017453; // dr = \pi/180.0 からラジアンへ
  
  glClear(GL_COLOR_BUFFER_BIT);
  for(i=0; i <= 15; i++) {
    t = (double)i*22.5*dr;
    ix = (int)(300*cos(t)) + r;
    iy = (int)(300*sin(t)) + r;
    linei(r, r, ix, iy);
  }

  circlei(r, r, 300);
  glFlush();
}

void myinit(void) {
  glClearColor(1.0, 1.0, 1.0, 0.0); // 背景色
  glColor3f(0.0, 0.0, 0.0);         // 描画色
}

void reshape(int w, int h) {
  h = (h == 0) ? 1 : h;
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, (float)w-1.0, 0, (float)h-1.0, -1.0, 1.0);
  glMatrixMode(GL_MODELVIEW);
}

/* ESCキーやEnterキーが入力されたらプログラムを終了する */
void keyboard(unsigned char key, int x, int y) {
  switch (key) {
  case 'q':
  case 'Q':
  case 27: // ESC
  case 13:
  case 10:
    exit(0);
  default:
    break;
  }
}

int main(int argc, char **argv) {
  glutInit(&argc, argv);
  glutInitWindowSize(620, 620);
  glutInitWindowPosition(10, 10);
  glutInitDisplayMode(GLUT_RGB);
  glutCreateWindow(argv[0]);
  myinit();
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  glutKeyboardFunc(keyboard);
  glutMainLoop();
  return 0;
}
