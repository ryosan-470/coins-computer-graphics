#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import math
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *


# 画面の位置(x,y)に点を描く
def ploti(x, y): 
    glBegin(GL_POINTS)
    glVertex2i(x, y)
    glEnd()


# 始点(x1, y1)から終点(x2, y2)まで線を描画する
def line(x1, y1, x2, y2):
    # 初期化
    x = x1
    y = y1
    ploti(x, y)

    dx = x2 - x1
    dy = y2 - y1
    e = -dx

    while (x < x2): 
        x += 1
        e = e + 2 * dy
        if (e > 0):
            y += 1 
            e = e - 2*dx
    
        if (dy >= 0 and dx >= dy):
            ploti(x, y)
  

def octant(r):
    x = 0
    y = r
    d = 3 - 2 * x
    
    while x < y:
        if d < 0:
            d = d + 4 * x + 6
        else:
            d = d + 4 * (x - y) + 10
            y -= 1

        x += 1
        ploti(x, y)


def display():
    m = 400
    dr = 0.017453 # dr = \pi/180.0 からラジアンへ
    glClear(GL_COLOR_BUFFER_BIT)

    for i in range(0,46,5):
        t = i*dr
        line(0, 0,(m * math.cos(t)), (m *  math.sin(t)))
  
    for i in range(20, 401, 20):
        octant(i)

    glFlush()


def init(): 
    glClearColor(1.0, 1.0, 1.0, 0.0) # 背景色
    glColor3f(0.0, 0.0, 0.0)         # 描画色


def reshape(w,  h): 
    if h == 0:
        h = 1

    glViewport(0, 0, w, h)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(-100, w-101, -100,  h-101.0, -1.0, 1.0)
    glMatrixMode(GL_MODELVIEW)


def keyboard(key, x, y):
    if key == 27 or key == 13 or key == 10:
        sys.exit(0)


def main(argv): 
    glutInit(argv)
    glutInitWindowSize(600, 600)
    glutInitWindowPosition(10, 10)
    glutInitDisplayMode(GLUT_RGB)
    glutCreateWindow(argv[0])
    init()
    glutDisplayFunc(display)
    glutReshapeFunc(reshape)
    glutKeyboardFunc(keyboard)
    glutMainLoop()


if __name__ == "__main__":
    main(sys.argv)
